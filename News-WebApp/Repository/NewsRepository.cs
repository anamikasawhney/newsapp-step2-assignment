﻿using Microsoft.EntityFrameworkCore;
using News_WebApp.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace News_WebApp.Repository
{
    /*
     This class contains the code for data storage interactions and methods 
     of this class will be used by other parts of the applications such
     as Controllers and Test Cases
     */
    public class NewsRepository
    {
        /* Declare a variable of NewsDbContext type to store 
         * all the news
        */

        /*
	        This method should accept News object as argument and add the new news object in
            NewsDbContext Object
	    */

        /*
        Use Async and await calls for AddNews(),GetAllNews(),GetNewsById() and RemoveNews()
        */


        /* Implement all the methods of respective interface asynchronously*/
        /* Implement AddNews method should accept News object as argument and add the new news object into 
              NewsDbContext Object*/

        /* Implement GetAllNews method should return all the news available in the the NewsDbContext Object*/

        /* Implement GetNewsById method should return the matching newsid details present in the 
         * the NewsDbContext Object*/

        /* Implement IsNewsExist method to check the news deatils exist or not*/

        /* Implement RemoveNews method should delete a specified news from the NewsDbContext Object*/
    }
}
